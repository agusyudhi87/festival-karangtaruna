<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $table = 'stage';
    protected $fillable = [
        'nama_stage',
        'deskripsi',
        'foto',
    ];
    
    // public function gueststar(){
    //     return $this->hasMany('App\Gueststar','id_stage');
    // }

    public function jadwal(){
        return $this->hasMany('App\Jadwal','id_stage');
    }
    public function getNamaStageAttribute($nama_stage){
        return ucwords($nama_stage);
    }
}
