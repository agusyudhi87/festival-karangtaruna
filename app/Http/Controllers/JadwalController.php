<?php

namespace App\Http\Controllers;

use App\Repositories\JadwalRepository;
use Illuminate\Http\Request;
use App\Jadwal;
use App\Stage;
use App\Gueststar;
use App\Http\Requests\JadwalRequest;
use Session;
use Carbon;

class JadwalController extends Controller
{
    protected $repository;

    public function __construct(JadwalRepository $repository)
    {
        //Instance repository UserRepository kedalam property $user
        $this->repository = $repository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_jadwal=Jadwal::all();
        return view('jadwal.index',compact('list_jadwal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_stage = Stage::pluck('nama_stage','id');
        $list_gueststar = Gueststar::pluck('nama','id');
        return view('jadwal.create',compact('list_stage','list_gueststar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->repository->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
