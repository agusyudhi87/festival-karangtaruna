<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Session;
use Storage;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_user = User::orderBy('level','asc')->Paginate(5);
        $jumlah_admin = User::where('level','Admin')->count();
        // return $jumlah_admin; dd;
        $jumlah_kt = User::where('level','Karang Taruna')->count();
        $jumlah_pengunjung = User::where('level','Pengunjung')->count();
        $total = User::count();        
        return view('user.index',compact('list_user','jumlah_admin','jumlah_kt','jumlah_pengunjung','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validasi = Validator::make($input, [
        'nama'     => 'required|max:255',
        'email'    => 'required|email|max:100|unique:users',
        'alamat'   => 'required',
        'password' => 'required|confirmed|min:6',
        'level'    => 'required|in:admin,karang taruna,pengunjung',
        'foto'     => 'sometimes|nullable|image|mimes:jpeg,jpg,png|max:2000'
        ]);
        // Hash password.
        $input['password'] = bcrypt($input['password']);
         // foto
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $ext = $foto->getClientOriginalExtension();
            if ($request->file('foto')->isValid()) {
                $foto_name = date('YmdHis').".$ext";
                $upload_path = 'fotoupload/user';
                $request->file('foto')->move($upload_path,$foto_name);
                $input['foto'] = $foto_name;
            }
        }

        User::create($input);
        Session::flash('flash_message', 'Data user berhasil disimpan.');

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $get = User::findOrFail($user);
        // dd($get);
        return view ('user.edit',compact('get'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        // dd($request->all());
        // User::where('id', $request->input('id'))->update([
        //     'nama' => $request->input('nama'),
        //   ]);
        $user = User::findOrFail($id);
          $input= $request->all();
        $validasi = Validator::make($input, [
            'nama'     => 'required|max:255',
            'email'    => 'required|email|max:100|unique:users,email,' . $input['id'],
            'alamat'   => 'required',
            'password' => 'sometimes|nullable|confirmed|min:6',
            'level'    => 'required',
            'foto'     => 'sometimes|nullable|image|mimes:jpeg,jpg,png|max:2000'
        ]);
        if ($validasi->fails()) {
            return redirect("user/$id/edit")
                    ->withErrors($validasi)
                    ->withInput();
        }
        if ($request->hasFile('foto')) {
            // Hapus foto lama jika ada foto baru.
            $exist = Storage::disk('foto')->exists($user->foto);
            if (isset($user->foto) && $exist) {
                $delete = Storage::disk('foto')->delete($user->foto);
            }

            // Upload foto baru.
            $foto = $request->file('foto');
            $ext  = $foto->getClientOriginalExtension();
            if ($request->file('foto')->isValid()) {
                $foto_name   = date('YmdHis'). ".$ext";
                $upload_path = 'fotoupload/user';
                $request->file('foto')->move($upload_path, $foto_name);
                $input['foto']=$foto_name;
            }
        }
        if ($request->filled('password')) {
            // Hash password.
            $input['password'] = bcrypt($input['password']);
        }
        else {
            // Hapus password (password tidak diupdate).
            $input = array_except($input, ['password']);
        }
        $user->update($input);
        // Flass message.
        Session::flash('flash_message', 'Data user berhasil di update.');
        return redirect ('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $exist = Storage::disk('user')->exists($user->foto);
            if (isset($user->foto) && $exist) {
                $delete = Storage::disk('user')->delete($user->foto);
            }
        $user->delete();
        return redirect('user');
    }
}
