<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gueststar;
use App\Stage;
use App\Http\Requests\GueststarRequest;
use Storage;
use Session;

class GueststarController extends Controller
{
    // menampilkan halaman utama gueststar
    public function index()
    {
        $gueststar_list = Gueststar::orderBy('nama','asc')
                                    // ->SimplePaginate(5);
                                    ->Paginate(9);
        $jumlah_gueststar= Gueststar::count();
        return view('gueststar.index',compact('gueststar_list','jumlah_gueststar'));
    }


    public function create()
    {
        $list_stage = Stage::pluck('nama_stage','id');
        return view('gueststar.create',compact('list_stage'));
    }


    public function store(GueststarRequest $request)
    {
        $input = $request->all();
           // foto
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $ext = $foto->getClientOriginalExtension();
            if ($request->file('foto')->isValid()) {
                $foto_name = date('YmdHis').".$ext";
                $upload_path = 'fotoupload';
                $request->file('foto')->move($upload_path,$foto_name);
                $input['foto'] = $foto_name;
            }
        }

        
        $gueststar = Gueststar::create($input);
        // Flass message.
        Session::flash('flash_message', 'Data gueststar berhasil di simpan.');
        return redirect ('gueststar');
    }


    public function show(Gueststar $gueststar)
    {
        return view('gueststar.show',compact('gueststar'));
    }


    public function edit(Gueststar $gueststar)
    {
        $list_stage = Stage::pluck('nama_stage','id');

        return view('gueststar.edit',compact('gueststar','list_stage'));
    }

    public function update(Gueststar $gueststar,GueststarRequest $request)
    {
        $input = $request->all();
        $validasi = Validator::make($input, [

            'email'    => 'required|email|max:100|unique:users,email,' . $input['id'],

        ]);
        if ($request->hasFile('foto')) {
            // Hapus foto lama jika ada foto baru.
            $exist = Storage::disk('foto')->exists($gueststar->foto);
            if (isset($gueststar->foto) && $exist) {
                $delete = Storage::disk('foto')->delete($gueststar->foto);
            }

            // Upload foto baru.
            $foto = $request->file('foto');
            $ext  = $foto->getClientOriginalExtension();
            if ($request->file('foto')->isValid()) {
                $foto_name   = date('YmdHis'). ".$ext";
                $upload_path = 'fotoupload';
                $request->file('foto')->move($upload_path, $foto_name);
                $input['foto']=$foto_name;
            }
        }
        $gueststar->update($input);
        // Flass message.
        Session::flash('flash_message', 'Data gueststar berhasil di update.');
        return redirect ('gueststar');
    }

    public function destroy(Gueststar $gueststar)
    {
        // hapus foto
        $exist = Storage::disk('foto')->exists($gueststar->foto);
            if (isset($gueststar->foto) && $exist) {
                $delete = Storage::disk('foto')->delete($gueststar->foto);
            }
        $gueststar->delete();
        Session::flash('flash_message', 'Data gueststar berhasil di hapus.');
        Session::flash('penting', true);
        return redirect('gueststar');
    }

    public function cari(Request $request) {

        $kata_kunci = $request->input('kata_kunci');
        $query = Gueststar::where('nama','LIKE','%'.
                $kata_kunci.'%');
        $gueststar_list = $query->paginate(6);
        $pagination = $gueststar_list->appends($request->except('page'));
        $jumlah_gueststar = $gueststar_list->total();
        return view('gueststar.index', compact('gueststar_list','kata_kunci','pagination','jumlah_gueststar'));

    }
    

}
