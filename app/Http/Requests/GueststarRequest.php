<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GueststarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'=>'required|string|max:15|',
            'deskripsi'=>'required|string|max:500',
            'foto' => 'sometimes|nullable|image|mimes:jpeg,jpg,png|max:2000',
            'id_stage'=>'sometimes|nullable|string'
        ];
    }
}
