<?php

namespace App\Repositories;

use App\Exceptions\GeneralException;
use App\Jadwal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Auth;


/**
 * Class JadwalRepositories.
 */
class JadwalRepository extends BaseRepository
{
    protected $model;

    public function __construct(Jadwal $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {   
        // dd($data);die;
        $dbtransaction = DB::transaction(function () use ($data) {
           
            $dateMulai = date($data['waktu_mulai']);
            $dateAkhir = date($data['waktu_selesai']);

            $jdlMulai = Jadwal::whereBetween('start', [$dateMulai, $dateAkhir])->get();
            $jdlAkhir = Jadwal::whereBetween('end', [$dateMulai, $dateAkhir])->get();
            // dd($dateMulai, $dateAkhir, $jdlMulai, $jdlAkhir);
            
            if($jdlAkhir->isEmpty() && $jdlAkhir->isEmpty()){
                $jdlEvent = Jadwal::create([
                                    'tanggal'=> $data['tanggal'],
                                    'start'=> $data['waktu_mulai'],
                                    'end'=> $data['waktu_selesai'],
                                    'aktivitas'=> $data['aktivitas'],
                                    'id_stage'=> $data['id_stage'],
                                    'id_gueststar'=> $data['id_gueststar'],
                                    'lokasi'=> $data['lokasi'],
                ]);
                return redirect('jadwal');
            }else{
                echo 'Waktu jadwal tidak tersedia!!!';
                return Redirect::route('jadwal.index');
            }
        });

        // return $this->model::find($id);
    }
}