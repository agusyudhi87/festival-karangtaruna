<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class FestivalKerambitanServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $halaman='';
        if (Request::segment(1) == 'gueststar'){
            $halaman = 'gueststar';
        }

        if (Request::segment(1) == 'stage'){
            $halaman = 'stage';
        }

        if (Request::segment(1) == 'karang_taruna'){
            $halaman = 'karang_taruna';
        }

        if (Request::segment(1) == 'user'){
            $halaman = 'user';
        }

        if (Request::segment(1) == 'jadwal'){
            $halaman = 'jadwal';
        }

        view()->share('halaman',$halaman);
    }
}
