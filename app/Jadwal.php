<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $fillable = [
        'tanggal',
        'start',
        'end',
        'aktivitas',
        'id_gueststar',
        'id_stage',
        'lokasi'
    ];
    public function gueststar(){
        return $this->belongsTo('App\Gueststar','id_gueststar');
    }

    public function stage(){
        return $this->belongsTo('App\Stage','id_stage');
    }
}
