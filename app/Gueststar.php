<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gueststar extends Model
{
    protected $table = 'gueststar';
    protected $fillable = [
        'nama',
        'deskripsi',
        'foto'
    ];
    // public function stage(){
    //     return $this->belongsTo('App\Stage','id_stage');
    // }

    public function jadwal(){
        return $this->hasOne('App\jadwal','id_gueststar');
    }
    public function getNamaAttribute($nama){
        return ucwords(strtolower($nama));
    }
}
