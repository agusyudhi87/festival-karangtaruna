<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});
// route untuk guest star
// Route::get('gueststar', 'GueststarController@index');
// Route::get('gueststar/create','GueststarController@create');
// Route::post('gueststar', 'GueststarController@store');
// Route::patch('gueststar/{gueststar}gueststar','GueststarController@update');
// Route::get('gueststar/{gueststar}/edit', 'GueststarController@edit');
// Route::get('gueststar/{gueststar}/show', 'GueststarController@show');
// Route::delete('gueststar/{gueststar}gueststar','GueststarController@destroy');
Route::get('gueststar/cari', 'GueststarController@cari');
Route::resource('gueststar','GueststarController');
Route::resource('stage','StageController');
Route::resource('karang_taruna','Karang_tarunaController');
Route::resource('user','UserController');
Route::resource('jadwal','JadwalController');
Route::resource('penampilan_kt','Penampilan_ktController');

Auth::routes();

