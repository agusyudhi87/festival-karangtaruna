<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJadwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->time('waktu_mulai');
            $table->time('waktu_selesai');
            $table->unsignedBigInteger('id_stage');
            $table->unsignedBigInteger('id_gueststar');
            $table->string('aktivitas');
            $table->string('lokasi');
            $table->timestamps();

            $table->foreign('id_stage')->references('id')->on('stage')->onDelete('cascade');
            $table->foreign('id_gueststar')->references('id')->on('gueststar')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwal', function (Blueprint $table) {
            $table->dropForeign('jadwal_id_stage_foreign');
            $table->dropForeign('jadwal_id_gueststar_foreign');
        });
        Schema::dropIfExists('jadwal');
    }
}
