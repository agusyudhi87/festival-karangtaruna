@extends('template')
@section('content')
<!-- Image and text -->
<nav class="navbar navbar-light bg-danger rounded-bottom">

    <img src="/gambarpemanis/karang_taruna.png" width="60%" height="40%" class="d-inline-block align-top" alt="">

</nav>
          
            <table class="table">
                <thead>
                  <tr>
                    
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Desa</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($list_karang_taruna as $karang_taruna)
                  <tr>
                    <td> {{$karang_taruna->nama}}</td>
                    <td> {{$karang_taruna->email}}</td>
                    <td> {{$karang_taruna->alamat}}</td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>

@endsection