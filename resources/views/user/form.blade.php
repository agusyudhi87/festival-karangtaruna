@if (isset($get))
{!! Form::hidden ('id',$get->id) !!}
@endif
<div class="col-md-9 form-group @error('nama') has-error @enderror">
     {!! Form::label ('nama','Nama:',['class' => 'control-label'])!!}
     {!! Form::text ('nama',null,['class' => 'form-control'])!!}
     @error('nama')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('email') has-error @enderror">
     {!! Form::label ('email','Email:',['class' => 'control-label'])!!}
     {!! Form::text ('email',null,['class' => 'form-control'])!!}
     @error('email')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('alamat') has-error @enderror">
     {!! Form::label ('alamat','Alamat:',['class' => 'control-label'])!!}
     {!! Form::text ('alamat',null,['class' => 'form-control'])!!}
     @error('alamat')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
@if ($errors->any())
     <div class="form-group {{ $errors->has('level') ? 'has-error' : 'has-success' }}">
@else
     <div class="form-group">
@endif
     {!! Form::label('level', 'Level:', ['class' => 'control-label']) !!}
<div class="radio">
     <label>{!! Form::radio('level', 'Admin') !!} Admin</label>
</div>
<div class="radio">
     <label>{!! Form::radio('level', 'Karang Taruna') !!} Karang Taruna</label>
</div>
<div class="radio">
     <label>{!! Form::radio('level', 'Pengunjung') !!} Pengunjung</label>
</div>
@if ($errors->has('level'))
     <span class="help-block">{{ $errors->first('level') }}</span>
@endif
</div>
<div class="col-md-9 form-group @error('password') has-error @enderror">
     {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
     {!! Form::password('password', ['class' => 'form-control']) !!}
     @error('password')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class=" col-md-9 form-group @error('password_confirmation') has-error @enderror">
     {!! Form::label('password_confirmation', 'Password Confirmation:', ['class' => 'control-label']) !!}
     {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
@if ($errors->has('password_confirmation'))
     <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
@endif
</div>
<div class=" col-md-9 form-group @error('foto') has-error @enderror">
     {!! Form::label('foto', 'Foto:') !!}
     {!! Form::file('foto') !!}
     </div>
     <div class="my-2">
     @if (isset($get))
          @if (isset($get->foto))
          <img src="{{asset('fotoupload/user/'.$get->foto)}}" class="" style="width: 20%">
          @endif
     @endif
</div>


{{-- -------submit--------- --}}
<div class="form-group">
     {!! Form::submit($submitButtonText,['class' => 'btn btn-primary '])!!}
</div>