@extends('template')
@section('content')
<div class="row" style="background-color: #FFDAB9">
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div>
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div> 
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div>
</div>
<div class="row bg-light border">
     <div class="col-md-12 border text-center pt-3">
          <h2>Edit Data User</h2>
          <p>Halaman Edit Data User|<strong>Festival Kerambitan </strong></p>
     </div>
     <div class="col py-4 border">
          {!! Form::model($get, ['method' => 'PATCH','files'=>true, 'action' => ['UserController@update', $get->id]]) !!}
          @include('user.form', ['submitButtonText' => 'Update User'])
          {!! Form::close() !!}
     {!! Form:: close()!!}
     </div>
</div>

@stop