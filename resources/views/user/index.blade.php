@extends('template')
@section('content')
<div id="user">
    {{-- @include('_partial.flash_message') --}}
    <div class="row" style="background-color: #FFDAB9">
        <div class="col">
            <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
        </div>
        <div class="col">
            <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
        </div> 
        <div class="col">
            <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
        </div>
        <div class="col-md-12 text-center">
            <p>Halaman Data User|<strong>Festival Kerambitan </strong></p>
        </div>
    </div>
    <div class="row bg-dark rounded-bottom ">
        <div class="col-md py-2">
            <div class="tombol-nav">
                <a href="user/create" class="btn btn-primary">Tambah User</a>
            </div>
        </div>
        <div class="col-md py-2">
            @include('user.form_pencarian')
        </div>
    </div>
    <div class="row mt-3 bg-light border">
        <div class="col-md-12 text-center pt-4" width="50%">
            <h2>Data User</h2>
        </div>
        <div class="col-md pt-3">
            <p>Admin: {{$jumlah_admin}}</p>
        </div>
        <div class="col-md pt-3">
            <p>Karang Taruna: {{$jumlah_kt}}</p>
        </div>
        <div class="col-md pt-3">
            <p>Karang Pengunjung: {{$jumlah_pengunjung}}</p>
        </div>
        <div class="col-md pt-3">
            <p>Total: {{$total}}</p>
        </div>
    </div>
    @if (count($list_user) > 0)
    <div class="row border">
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Level</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0; ?>
            <?php foreach($list_user as $user): ?>
            <tr>
                <td class="table-light">{{ ++$i }}</td>
                <td class="table-light">{{ $user->nama }}</td>
                <td class="table-light">{{ $user->email }}</td>
                <td class="table-light">{{ $user->level }}</td>
                <td class="table-light">
                    <a href="user/{{$user->id}}/edit" class="btn btn-info btn-sm">Show</a>
                </td>
            </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    @else
        <p>Tidak ada data user.</p>
    @endif
    </div>
    <div class="row my-1">
        <div class="col">
            <ul class="pagination py-1 px-1 bg-dark">
                {{$list_user->links()}}
            </ul>
        </div>
    </div>
</div> <!-- / #kelas -->
@stop