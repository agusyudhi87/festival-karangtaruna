@extends('template')
@section('content')
<div class="row" style="background-color: #FFDAB9">
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div>
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div> 
     <div class="col">
          <img src="/gambarpemanis/user.png" class="rounded mx-auto d-block pt-3" width="50%">
     </div>
</div>
<div class="row border bg-light">
     <div class="col-md-12 text-center pt-3">
          <h2>Tambah Data User</h2>
          <p>Halaman Tambah Data User|<strong>Festival Kerambitan </strong></p>
     </div>
     <div class="col-md-12 py-3">
          {!! Form::open(['url' => 'user','files'=>true]) !!}
          @include('user.form', ['submitButtonText' => 'Tambah User'])
          {!! Form::close() !!}
     </div>
</div>
@stop
