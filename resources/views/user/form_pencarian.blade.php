<div id="pencarian">
     {!! Form::open(['url'=>'gueststar/cari','method'=>'get']) !!}
          <div class="input-group">
               {!! Form::text ('kata_kunci',(!empty($kata_kunci))?
               $kata_kunci : null, ['class'=>'form-control mx-2 rounded', 'placeholder'=>'masukan nama user'])!!}
               <span class="input-group-btn">
                    {!! Form::button ('Cari',['class' => 'btn btn-outline-success my-2 my-sm-0','type'=>'submit'])!!}  
               </span>
          </div>
     {!!Form::close()!!}
     </div>