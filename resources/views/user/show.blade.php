@extends('template')
@section('content')

<div class="row">
     <div class="col mt-5">
          <div class="card mx-auto d-block" style="width: 35rem;">
               <div class="row">
                    <div class="col pt-2 ">
                         @if (isset($user->foto))
                         <img src="{{asset('fotoupload/user/'.$user->foto)}}" class=" bg-dark rounded mx-auto d-block" style="max-width: 16rem;">
                         @endif
                    </div>
               </div>
               <ul class="list-group list-group-flush">
                    <li class="list-group-item text-center"><strong>Data User</strong></li>
                    <li class="list-group-item"><small class="text-muted">Nama :</small><br> {{$user->nama}}</li>
                    <li class="list-group-item"><small class="text-muted">Email :</small><br> {{$user->email}}</li>
                    <li class="list-group-item"><small class="text-muted">Alamat :</small><br> {{$user->alamat}}</li>
                    <li class="list-group-item"><small class="text-muted">Level :</small><br>{{$user->level}}</li>
               </ul>
               <div class="card-body">
                    {!! Form::model ($user,['class'=>'d-inline','method'=>'delete','action'=>
                    ['UserController@destroy',$user->id,]])!!}
                         <button type="submit" velue="delete" class="btn btn-danger btn-sm">Delete</button>
                    {!! form:: close()!!}
                    {{ link_to('user/' . $user->id . '/edit', 'Edit', ['class' => 'd-inline btn btn-warning btn-sm']) }}
                    
               </div>
          </div>
     </div>
</div>

@stop