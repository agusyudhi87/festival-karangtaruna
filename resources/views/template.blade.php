<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('bootstrap-4.4.1-dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/style.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('../css/jquery.timepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('../css/bootstrap-datepicker.css') }}">

        <title>Festival Kerambitan</title>
    </head>
    <body class="background">
            @include('navbar')
        <div class="container">
            <div class="tinggi">
                @yield('content')
            </div>
        </div>
            @include('footer')

    </body>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{asset('js/jquery_2_2_1.min.js')}}"></script>
    <script src="{{asset('bootstrap-4.4.1-dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/festivalkerambitan.js')}}"></script>
    <script src="{{asset('../js/jquery.timepicker.js') }}"></script>
    <script src="{{asset('../js/bootstrap-datepicker.js') }}"></script>
    <script>
        $(document).ready(function(){
            //  console.log('alert');
            $('#start-time').timepicker({
                'timeFormat': 'H:i:s'
            });
            $('#end-time').timepicker({
                'timeFormat': 'H:i:s'
            });
            $('#datepairExample').datepicker({
                format: 'yyyy-mm-dd',
                'autoclose': true
            });
            
        });
    </script>
</html>