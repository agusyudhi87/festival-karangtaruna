@extends('template')
@section('content')
<h2>edit stage</h2>
<div class="col-md bg-light px-5 py-3 my-2 border">
     {!! Form::model ($stage,['method'=>'patch','files'=>true,'action'=>
     ['StageController@update',$stage->id]])!!}
          @include('stage.form',['submitButtonText'=>'Update']) 
     {!! Form:: close()!!}
</div>

@endsection