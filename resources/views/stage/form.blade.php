<div class="col-md-9 form-group @error('nama_stage') has-error @enderror">
     {!! Form::label ('_stage','Nama Stage:',['class' => 'control-label'])!!}
     {!! Form::text ('nama_stage',null,['class' => 'form-control'])!!}
     @error('nama_stage')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('deskripsi') has-error @enderror"">
     {!! Form::label ('deskripsi','Deskripsi:',['class' => 'control-label'])!!}
     {!! Form::textarea ('deskripsi',null,['class' => 'form-control','style'=>'height:8rem'])!!}
     @error('deskripsi')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class=" col-md-9 form-group @error('foto') has-error @enderror"">
{!! Form::label('foto', 'Foto:') !!}
{!! Form::file('foto') !!}
</div>
<div class="my-2">
@if (isset($stage))
     @if (isset($stage->foto))
     <img src="{{asset('fotoupload/'.$stage->foto)}}" class="" style="width: 20%">
     @endif
@endif
</div>


{{-- -------submit--------- --}}
<div class="form-group">
     {!! Form::submit ($submitButtonText,['class' => 'btn btn-primary '])!!}
</div>