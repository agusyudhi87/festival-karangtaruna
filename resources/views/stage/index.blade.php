@extends('template')
@section('content')
<div id="stage">
     <div class="row text-center text-light border" style="background-image: url(/gambarpemanis/dark-honeycomb.png)">
          <div class="col-md-12">
               <h2 class="display-2 my-2">STAGE</h2>
          </div>
          <div class="col-md-12">
               <p>Panggung acara pada pagelaran <strong>Festival Kerambitan</strong></p>
          </div>
          <div class="col-md-12">
               <a href="{{url('/stage/create')}}" class="btn btn-primary d-block">Tambah Stage</a>
          </div>
     </div>
     <div class="row pt-5 mt-5 pb-5 bg-light border border">
          @foreach ($stage_list as $stage)
          <div class="col-md py-5">
               <div class="card mb-3 rounded mx-auto d-block" style="width: 35rem">
                    @if (isset($stage->foto))
                    <img src="{{asset('fotoupload/'.$stage->foto)}}" class="card-img" style="height: 22rem">
                    @endif
               </div>
          </div>
          <div class="col-md bg-light mr-3 my-5">
               <div class="card-body ">
                    <h5 class="card-title">{{$stage->nama_stage}}</h5>
                    <hr class="my-2">
                    <p class="text-muted"> <small> Deskripsi :</small> </p>
                    <p class="card-text">{{$stage->deskripsi}}</p>
                    <hr class="my-2">
                         {!! Form::model ($stage,['class'=>'d-inline','method'=>'delete','action'=>
                         ['StageController@destroy',$stage->id,]])!!}
                         <button type="submit" velue="simpan" class="btn btn-danger btn-sm">Delete</button>
                         {!! form:: close()!!}
                         <a href="/stage/{{$stage->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
               </div>
          </div>
          <hr>
          @endforeach
     </div>
</div>

@endsection