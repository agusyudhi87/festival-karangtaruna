
@extends('template')
@section('content')
<h2>ini adalah jadwal</h2>
<a href="{{url('jadwal/create')}}" class="btn btn-primary">tambah</a>
  <table class="table table-striped table-bordered">
    <thead class="bg-dark text-light">
      <tr>  
        <th scope="col">Tanggal</th>
        <th scope="col">Waktu</th>
        <th scope="col">Aktivitas</th>
        <th scope="col">Guest Star</th>
        <th scope="col">Stage</th>
        <th scope="col">Lokasi</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($list_jadwal as $jadwal)
      <tr>
        <td>{{$jadwal->tanggal}}</td>
        <td>{{$jadwal->waktu_mulai}} - {{$jadwal->waktu_selesai}} WITA</td>
        <td>{{$jadwal->aktivitas}}</td>
        <td>{{$jadwal->gueststar->nama}}</td>
        <td>{{$jadwal->stage->nama_stage}}</td>
        <td>{{$jadwal->lokasi}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

@endsection