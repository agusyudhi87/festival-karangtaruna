
<div class="form-group col-md-3">
     {!! Form::label ('tanggal','Tanggal:',['class' => 'control-label'])!!}
     {!! Form::text('tanggal',null,['class' => 'form-control', 'id'=>'datepairExample'])!!}
     @error('tanggal_')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
     <div class="form-group col-md-3">
          {!! Form::label ('waktu_mulai','Waktu Mulai:',['class' => 'control-label'])!!}
          {!! Form::text ('waktu_mulai',null,['class' => 'form-control','id'=> 'start-time'])!!}
     </div>
     <div class="form-group col-md-3">
          {!! Form::label ('waktu_selesai','Waktu Selesai:',['class' => 'control-label'])!!}
          {!! Form::text ('waktu_selesai',null,['class' => 'form-control','id'=> 'end-time'])!!}
     </div>
<div class="col-md-9 form-group @error('aktivitas') has-error @enderror">
     {!! Form::label ('aktivitas','Aktivitas:',['class' => 'control-label'])!!}
     {!! Form::text ('aktivitas',null,['class' => 'form-control'])!!}
     @error('aktivitas')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>

<div class="col-md-9 form-group @error('id_stage') has-error @enderror">
     {!! Form::label ('id_stage','stage:',['class' => 'control-label'])!!}
     @if(count($list_stage)>0)
     {!! Form::select ('id_stage',$list_stage,null,['class' => 'form-control' ,'id'=>'id_stage','placeholder'=>'pilih stage'])!!}
     @else
     <p>Belum ada stage, silahkan isi data stage! </p>
     @endif
     @error('id_stage')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('id_gueststar') has-error @enderror">
     {!! Form::label ('id_gueststar','gueststar:',['class' => 'control-label'])!!}
     @if(count($list_gueststar)>0)
     {!! Form::select ('id_gueststar',$list_gueststar,null,['class' => 'form-control' ,'id'=>'id_gueststar','placeholder'=>'pilih gueststar'])!!}
     @else
     <p>Belum ada gueststar, silahkan isi data gueststar! </p>
     @endif
     @error('id_gueststar')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('lokasi') has-error @enderror">
     {!! Form::label ('lokasi','Lokasi:',['class' => 'control-label'])!!}
     {!! Form::text ('lokasi',null,['class' => 'form-control'])!!}
     @error('lokasi')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>

{{-- -------submit--------- --}}
<div class="form-group">
     {!! Form::submit ($submitButtonText,['class' => 'btn btn-primary '])!!}
</div>

