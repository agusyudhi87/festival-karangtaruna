@extends('template')
@section('content')
<div id="gueststar" class="border-dark">

{{-- judul bagian atas --}}
<section id="judul">
<div class="row" style="background-image: url(/gambarpemanis/dark-honeycomb.png)">
     <div class="col-md">
          <div class="col-md py-4 text-center text-light">
               <h2><strong>GUEST STAR</strong> </h2>
               <p>Halaman Guest Star|<strong class="text-warning">Festival Kerambitan </strong></p>
               <p><small>Festival Kerambitan mencoba memberikan kontribusi lewat undangan bintang tamu mereka</small></p>
               <a href="{{url('gueststar/create')}}" class="btn btn-primary">tambah</a>
          </div>
     </div>
</div>
<div class="row rounded-bottom" style="background-color: #FFDAB9">
     <div class="col">
          <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
     </div>
     <div class="col">
          <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
     </div>
     <div class="col">
          <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
     </div>
</div>
</section>
{{-- akhir judul bagian atas --}}
<section id="isi">
     <div class="row mt-2 bg-light border">
          <div class="col-md pt-3">
               <h5>Guest Star: {{$jumlah_gueststar}}</h5>
          </div>
          <div class="col-md pt-2 mb-2">
               @include('gueststar.form_pencarian')
          </div>
     </div>
     @if (count($gueststar_list)>0)
     <div class="row bg-light py-4 border">
          <div class="col-md-12">
               @include('_partial.flash_message')
          </div>
          @foreach ($gueststar_list as $gueststar)
          <div class="col-md-4 my-3">
               <div class="card mx-auto d-block" style="width: 18rem;">
                    @if (isset($gueststar->foto))
                    <img src="{{asset('fotoupload/'.$gueststar->foto)}}" style="height: 13rem" class="card-img-top">
                    @endif
                    <div class="card-body">
                         <h5 class="card-title">{{$gueststar->nama}}</h5>
                         <a href="gueststar/{{$gueststar->id}}" class="btn btn-info">Show</a>
                    </div>
               </div>
          </div>
          @endforeach
     </div>
     @else
     <div class="col">
          <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
     </div>
     @endif
     <div class="row my-1">
          <div class="col">
               <ul class="pagination py-1 px-1 bg-dark">
                    {{$gueststar_list->links()}}
               </ul>
          </div>
     </div>
</section>
</div>
@stop