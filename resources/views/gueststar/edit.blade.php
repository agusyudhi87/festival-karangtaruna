@extends('template')
@section('content')
<section id="gueststar">
     <div class="row rounded-bottom py-3" style="background-color: #FFDAB9">
          <div class="col">
               <img src="/gambarpemanis/gueststar.png" class="rounded mx-auto d-block" width="40%">
          </div>
          <div class="col">
               <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
          </div>
          <div class="col">
               <img src="/gambarpemanis/gueststar.png" class="rounded mx-auto d-block" width="40%">
          </div>
     </div>
</section>
<div class="row bg-light">
     <div class="col-md-12 border py-3 text-center">
          <h2>Edit Data Guest Star</h2>
          <p><small>Edit Guest Star <strong>Festival Kerambitan</strong> </small></p>
     </div>
     <div class="col-md border px-5 py-3">
          {!! Form::model ($gueststar,['method'=>'patch','files'=>true,'action'=>
          ['GueststarController@update',$gueststar->id]])!!}
               @include('gueststar.form',['submitButtonText'=>'Update']) 
          {!! Form:: close()!!}
     </div>
</div>


@endsection