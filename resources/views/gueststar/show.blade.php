@extends('template')
@section('content')
<div class="container pt-5 mt-5 pb-5 bg-light">
     <div class="row">
          <div class="col-md">
               <div class="card mb-3 rounded mx-auto d-block" style="width: 100%;">
                    @if (isset($gueststar->foto))
                    <img src="{{asset('fotoupload/'.$gueststar->foto)}}" class="card-img">
                    @endif
               </div>
          </div>
          <div class="col-md">
               <div class="card-body">
                    <h5 class="card-title">{{$gueststar->nama}}</h5>
                    <hr class="my-2">
                    <small class="text-muted">Deskripsi :</small>
                    <p class="card-text">{{$gueststar->deskripsi}}</p>
                    <hr class="my-2">
                         {!! Form::model ($gueststar,['class'=>'d-inline','method'=>'delete','action'=>
                         ['GueststarController@destroy',$gueststar->id,]])!!}
                         <button type="submit" velue="delete" class="btn btn-danger btn-sm">Delete</button>
                         {!! form:: close()!!}
                         <a href="/gueststar/{{$gueststar->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                         <a href="{{url('/gueststar')}}">Kembali</a>
               </div>
          </div>
     </div>
</div>
@stop