<div class="col-md-9 form-group @error('nama') has-error @enderror">
     {!! Form::label ('nama','Nama:',['class' => 'control-label'])!!}
     {!! Form::text ('nama',null,['class' => 'form-control'])!!}
     @error('nama')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
<div class="col-md-9 form-group @error('deskripsi') has-error @enderror">
     {!! Form::label ('deskripsi','Deskripsi:',['class' => 'control-label'])!!}
     {!! Form::textarea ('deskripsi',null,['class' => 'form-control','style'=>'height:8rem'])!!}
     @error('deskripsi')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div>
{{-- <div class="col-md-9 form-group @error('id_stage') has-error @enderror">
     {!! Form::label ('id_stage','stage:',['class' => 'control-label'])!!}
     @if(count($list_stage)>0)
     {!! Form::select ('id_stage',$list_stage,null,['class' => 'form-control' ,'id'=>'id_stage','placeholder'=>'pilih stage'])!!}
     @else
     <p>Belum ada stage, silahkan isi data stage! </p>
     @endif
     @error('id_stage')<span class= "help-block text-danger">{{$message}}</span>@enderror
</div> --}}
<div class=" col-md-9 form-group @error('foto') has-error @enderror">
{!! Form::label('foto', 'Foto:') !!}
{!! Form::file('foto') !!}
</div>
<div class="my-2">
@if (isset($gueststar))
     @if (isset($gueststar->foto))
     <img src="{{asset('fotoupload/'.$gueststar->foto)}}" class="" style="width: 20%">
     @endif
@endif
</div>


{{-- -------submit--------- --}}
<div class="form-group">
     {!! Form::submit ($submitButtonText,['class' => 'btn btn-primary '])!!}
</div>