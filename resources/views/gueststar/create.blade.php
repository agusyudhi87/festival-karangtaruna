@extends('template')
@section('content')
<section id="judul">
     <div class="row rounded-bottom py-3" style="background-color: #FFDAB9">
          <div class="col">
               <img src="/gambarpemanis/gueststar.png" class="rounded mx-auto d-block" width="40%">
          </div>
          <div class="col">
               <img src="/gambarpemanis/gueststar1.png" class="rounded mx-auto d-block" width="50%">
          </div>
          <div class="col">
               <img src="/gambarpemanis/gueststar.png" class="rounded mx-auto d-block" width="40%">
          </div>
     </div>
</section>
<div class="row bg-light ">
     <div class="col-md pt-5 border text-center">
          <h2>Tambah Data Guest Star</h2>
          <p><small>Isi form untuk menambahkan data guest star yang akan mengisi acara <strong>Festival Kerambitan</strong> </small></p>
     </div>
     <div class="col-md-12 px-5 py-3 border">
          {!! Form::open (['url'=>'gueststar','files'=>true])!!}
               @include('gueststar.form', ['submitButtonText' => 'Tambah Gueststar'])
          {!! Form:: close()!!}
     </div>
</div>


@endsection