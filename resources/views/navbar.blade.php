<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
     <a class="navbar-brand" href="/">Festival<strong class="">Kerambitan</strong></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
     </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
               @if(!empty($halaman) && $halaman == 'gueststar')
               <li class="nav-item active">
                    <a class="nav-link" href="{{url('gueststar')}}">Guest Star <span class="sr-only">(current)</span></a>
               </li>@else
               <li class="nav-item">
                    <a class="nav-link" href="{{url('gueststar')}}">Guest Star <span class="sr-only">(current)</span></a>
               </li>
               @endif
               @if(!empty($halaman) && $halaman == 'stage')
               <li class="nav-item active">
                    <a class="nav-link" href="{{url('stage')}}">Stage <span class="sr-only">(current)</span></a>
               </li>@else
               <li class="nav-item">
                    <a class="nav-link" href="{{url('stage')}}">Stage <span class="sr-only">(current)</span></a>
               </li>
               @endif
               @if(!empty($halaman) && $halaman == 'karang_taruna')
               <li class="nav-item active">
                    <a class="nav-link" href="{{url('karang_taruna')}}">Karang Taruna <span class="sr-only">(current)</span></a>
               </li>@else
               <li class="nav-item">
                    <a class="nav-link" href="{{url('karang_taruna')}}">Karang Taruna <span class="sr-only">(current)</span></a>
               </li>
               @endif
               {{-- @if (Auth::user()->level == "Admin") --}}
                    @if(!empty($halaman) && $halaman == 'user')
                    <li class="nav-item active">
                         <a class="nav-link" href="{{url('user')}}">User<span class="sr-only">(current)</span></a>
                    </li>@else
                    <li class="nav-item">
                         <a class="nav-link" href="{{url('user')}}">User<span class="sr-only">(current)</span></a>
                    </li>
                    @endif
               {{-- @endif --}}
               @if(!empty($halaman) && $halaman == 'jadwal')
               <li class="nav-item active">
                    <a class="nav-link" href="{{url('jadwal')}}">Jadwal<span class="sr-only">(current)</span></a>
               </li>@else
               <li class="nav-item">
                    <a class="nav-link" href="{{url('jadwal')}}">Jadwal<span class="sr-only">(current)</span></a>
               </li>
               @endif
               @if(!empty($halaman) && $halaman == 'penampilan_kt')
               <li class="nav-item active">
                    <a class="nav-link" href="{{url('penampilan_kt')}}">Penampilan<span class="sr-only">(current)</span></a>
               </li>@else
               <li class="nav-item">
                    <a class="nav-link" href="{{url('penampilan_kt')}}">Penampilan<span class="sr-only">(current)</span></a>
               </li>
               @endif
               <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">a</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                    </div>
               </li>
          </ul>
           <!-- Link Login / Logout -->
     <ul class="nav navbar-nav navbar-right">
          {{-- {{ Auth::user()->nama }} --}}
          @if (Auth::check())
          <li class="nav-item dropdown">
               <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->nama }} <span class="caret"></span>
               </a>

               <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                    </form>
               </div>
          </li>
          @else
          <li class="nav-item">
               <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @endif
     </ul><!-- /.logout link -->
          </div>
</nav>